document.addEventListener('DOMContentLoaded', function () {
  // Get references to the text areas, result div, and differences counter
  const text1Input = document.getElementById('text1');
  const text2Input = document.getElementById('text2');
  const resultDiv = document.getElementById('result');
  const differencesCounter = document.getElementById('differencesCounter');

  // Get the compare button and add a click event listener
  const compareButton = document.getElementById('compareButton');
  compareButton.addEventListener('click', compareText);

  function compareText() {
    // Get the text content from the text areas
    const text1 = text1Input.value;
    const text2 = text2Input.value;

    // Perform text comparison and highlight differences
    const { differences, count } = highlightDifferences(text1, text2);

    // Display the highlighted differences and the count in the result div
    resultDiv.innerHTML = differences;
    differencesCounter.textContent = `Number of Differences: ${count}`;
  }

  // Function to find and highlight differences between two strings
  function highlightDifferences(text1, text2) {
    const lines1 = text1.split('\n');
    const lines2 = text2.split('\n');

    let differences = '';
    let count = 0;

    for (let i = 0; i < Math.max(lines1.length, lines2.length); i++) {
      if (lines1[i] !== lines2[i]) {
        differences += '<div class="difference">';
        if (lines1[i]) {
          differences += '<div class="removed">' + lines1[i] + '</div>';
        }
        if (lines2[i]) {
          differences += '<div class="added">' + lines2[i] + '</div>';
        }
        differences += '</div>';
        count++;
      }
    }

    return { differences, count };
  }
});